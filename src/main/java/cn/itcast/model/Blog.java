package cn.itcast.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("blog")
@Data
public class Blog {
    @Id
    private String id;
    private String title;
    private String by;
    private String url;
    private List<String> tags;
    private int likes;
}