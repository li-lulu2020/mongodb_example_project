package cn.itcast.controller;

import cn.itcast.dao.BlogDao;
import cn.itcast.model.Blog;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/blog")
public class WebController {
    @Resource
    private BlogDao blogDao;

    @RequestMapping("/{id}")
    @ResponseBody
    public String getBlogInfo(@PathVariable("id") String id) {
        Blog blog = blogDao.findByID(id);
        if (null == blog) {
            return "访问的数据不存在";
        }
        return JSON.toJSONString(blog);
    }

    @RequestMapping("/add")
    @ResponseBody
    public String addBlog(@RequestBody Blog blog) {
        blogDao.insert(blog);
        return JSON.toJSONString(blog);
    }

    public void batchAdd() {

    }
}